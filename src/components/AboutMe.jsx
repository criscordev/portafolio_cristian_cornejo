import React from 'react'
import '../styles/aboutMe.css'
import '../styles/loop.scss'
import developer_img from '../images/perfil2.jpg'
import CodeListItem from './shared/CodeListItem'
import Icon from './shared/Icon'

const AboutMe = ({ data, technologies }) => {
  return (
    <div id='about-me' className='about-me section'>
      <div className='container'>
        <h1>Acerca de mí</h1>
        <div className='line'></div>
        <div className='row content-center'>
          <div className='container'>
            <div className='row content-center'>
              <img
                loading='lazy'
                className='banner__img'
                src={developer_img}
                alt='user'
              ></img>
            </div>
            <div className='row content-center'>
              <ul className='about-me__list'>
                {data.map((text, i) => (
                  <li className='about-me__item' key={i}>
                    <CodeListItem text={text} />
                  </li>
                ))}
              </ul>
            </div>
          </div>
          <div>
            <div className='row content-center about-me__list'>
              {technologies.map((tech, i) => (
                <Icon
                  key={i}
                  svg={tech}
                  classes='svg-icon__svg mt-3 ml-2'
                  title={tech}
                ></Icon>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default AboutMe
