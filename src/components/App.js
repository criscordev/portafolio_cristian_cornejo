import React, { lazy, Suspense } from 'react'
import Header from './Header'
import Home from './Home'
import Contact from './Contact'
import Footer from './Footer'
import { dataAboutMe, technologies, projectsData } from '../data'
function App () {
  const Projects = lazy(() => import('./Projects'))
  const AboutMe = lazy(() => import('./AboutMe'))
  return (
    <div className='App'>
      <div>
        <Header />
        <Home technologies={technologies} />
      </div>
      <Suspense fallback={<div>Cargando...</div>}>
        <Projects projects={projectsData} />
      </Suspense>
      <Suspense fallback={<div>Cargando...</div>}>
        <AboutMe data={dataAboutMe} technologies={technologies} />
      </Suspense>
      <div>
        <Contact />
        <Footer />
      </div>
    </div>
  )
}

export default App
