import React from 'react'
import '../styles/footer.css'
const Footer = () => {
  return (
    <div className='footer-section'>
      <div className='header-bar'></div>
      <div className='footer__text'>
        <span>
          Creado por Cristian Cornejo. Hecho con React.{' '}
          <a className='footer__link'
            target='_blank'
            rel='noreferrer'
            href='https://gitlab.com/criscordev/portafolio_cristian_cornejo'
          >
            Ver el repositorio aqui.
          </a>
        </span>
      </div>
    </div>
  )
}

export default Footer
