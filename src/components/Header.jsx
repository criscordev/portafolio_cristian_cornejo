import React, { useState } from 'react'
import { Link } from 'react-scroll'
import '../styles/header.css'
import MenuIcon from '@material-ui/icons/Menu'
const Header = () => {
  const [check, setCheck] = useState(false)
  return (
    <div className='header-section'>
      <div className='colmillo'>
        <h5 className='header__title'>CRISTIAN DANIEL CORNEJO PEREIRA</h5>
        <ul className={`header__nav ${check ? 'mostrar' : ''}`}>
          <li>
            <Link
              smooth={true}
              duration={500}
              className='header__link'
              onClick={() => setCheck(false)}
              to='about-me'
            >
              Sobre mí
            </Link>
          </li>
          <li>
            <Link
              smooth={true}
              duration={500}
              className='header__link'
              onClick={() => setCheck(false)}
              to='projects'
            >
              Proyectos
            </Link>
          </li>
          <li>
            <Link
              htmlFor='check'
              smooth={true}
              duration={500}
              className='header__link'
              onClick={() => setCheck(false)}
              to='contact'
            >
              Contacto
            </Link>
          </li>
          <li>
            <a
              className='header__buttom'
              href='/CV.pdf'
              target='_blank'
              onClick={() => setCheck(false)}
            >
              Descargar CV
            </a>
          </li>
        </ul>

        <label className='menu-label' onClick={() => setCheck(!check)}>
          <MenuIcon className='header__menu-icon' />
        </label>
      </div>
      <div className='header-bar'></div>
    </div>
  )
}

export default Header
