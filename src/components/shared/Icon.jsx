import React from 'react'
import svgs from '../../icons'
import '../../styles/contact.css'
const Icon = ({ svg, classes, click, title }) => {
  const svgRender = svgs[svg] || svgs.default
  return (
    <div className='svg-icon'>
      <div className='tp'>{title}</div>
      <svg
        viewBox={svgRender.viewBox}
        className={classes}
        title={title}
        xmlns='http://www.w3.org/2000/svg'
      >
        {svgRender.svg}
      </svg>
    </div>
  )
}

export default Icon
