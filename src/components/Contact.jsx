import React from 'react'
import { Facebook, LinkedIn, WhatsApp, Mail } from '@material-ui/icons'
const Contact = () => {
  return (
    <div id='contact' className='contact section'>
      <div className='container'>
        <h1>Contacto</h1>
        <div className='line'></div>
        <div className='row content-center'>
          <div>
            <p className='contact__text'>
              Si estás interesado en que sea parte de tu equipo no dudes en
              contacterme por mis redes:
            </p>
          </div>
        </div>
        <div className='row content-center'>
          <div id='social-media'>
            <a
              aria-label='facebook'
              className='custom-color color-facebook'
              target='_blank'
              rel='noreferrer'
              href='https://www.facebook.com/crisdan0'
            >
              <Facebook fontSize='large' />
            </a>
            <a
              aria-label='whatsapp'
              className='custom-color color-whatsapp'
              target='_blank'
              rel='noreferrer'
              href='https://wa.me/51982932439'
            >
              <WhatsApp fontSize='large' />
            </a>
            <a
              aria-label='linkedin'
              className='custom-color color-linkedin'
              target='_blank'
              rel='noreferrer'
              href='https://www.linkedin.com/in/cristian-daniel-cornejo-pereira-41b70a207/'
            >
              <LinkedIn fontSize='large' />
            </a>
          </div>
        </div>
        <div className='row content-center'>
          <div>
            <p className='contact__text'>O también por correo:</p>
          </div>
        </div>
        <div className='row content-center'>
          <div>
            <a
              className='contact__button'
              href='mailto:cristian.cornejo1@outlook.com'
            >
              {' '}
              <Mail className='contact__icon' /> CONTÁCTAME
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Contact
