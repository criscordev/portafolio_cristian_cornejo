import poke_img from './images/poke_img.png'
import yomero from './images/yomero.png'
import splitter from './images/splitter.png'

class Image {
  constructor (live, type, img, title, text, tecs, repository, bg) {
    this.live = live
    this.type = type
    this.img = img
    this.title = title
    this.text = text
    this.tecs = tecs
    this.repository = repository
    this.bg = bg
  }
}

const projects = {
  aulaVirtual: new Image(
    null,
    'back',
    null,
    'API REST para Aula Virtual utilizando GraphQL y MongoDB',
    null,
    null,
    'https://gitlab.com/Cristian_Cornejo/aulavirtual-graphql',
    1
  ),
  market: new Image(
    null,
    'back',
    null,
    'API REST para Market utilizando Spring y PostgreSql',
    null,
    null,
    'https://gitlab.com/Cristian_Cornejo/market-api',
    2
  ),
  pokedex: new Image(
    'https://pokedex-app-react-redux.web.app/',
    'front',
    poke_img,
    'Pokedex App',
    `Aplicación web para buscar a los pokemones de primera
    generacion ver sus atributos y habilidades y agregarlos a tu
    equipo.`,
    'React | Redux',
    'https://gitlab.com/Cristian_Cornejo/pokedex-app-redux',
    null
  ),
  portafolio: new Image(
    'https://portafolio-cris-cor.web.app/',
    'front',
    yomero,
    'Portafolio',
    `Portafolio web realizado para mi presentación personal, proyectos e información sobre mí.`,
    'React',
    'https://gitlab.com/criscordev/portafolio_cristian_cornejo',
    null
  ),
  splitter: new Image(
    'https://cristian-cornejo.github.io/tip-calculator-app/',
    'front',
    splitter,
    'Splitter',
    `Aplicación para dividir la cuenta y saber cuanta propina corresponde dar. reto propuesto por frontend mentor`,
    'Vue 3',
    'https://github.com/Cristian-Cornejo/tip-calculator-app',
    null
  )
}

export default projects
