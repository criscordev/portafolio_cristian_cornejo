const dataAboutMe = [
  `Soy un Ingeniero de software egresado de la
  Universidad Nacional Mayor de San Marcos.`,
  `Aficionado al desarollo web con
  tecnologias de vanguardia como Angular, React, Redux,
  GraphQL, Mongo.`,
  `Con mas de 1 año experencia trabajando
  con Spring, Batis, Thymeleaf y Java.`
]

const technologies = [
  'react',
  'angular',
  'css',
  'html',
  'js',
  'ts',
  'java',
  'spring'
]

const projectsData = ['portafolio','pokedex','splitter']
//, 'aulaVirtual', 'market'
export { dataAboutMe, technologies, projectsData }
